using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class LevelManager : Singleton<LevelManager>
{
    string m_sStartLevelTouchMessage = "Touch the screen\r\nto Start";
    string m_sEndLevelTouchMessage = "Touch the screen\r\nto continue";
    string m_sGameOverTouchMessage = "Touch the screen\r\nto return to\r\nMain menu";

    string m_sGameOverWinMessage = "Congratulations";
    string m_sGameOverLoseMessage = "Game Over";

    [SerializeField, FoldoutGroup("Audio")] AudioClip m_oVictoryMusic;
    [SerializeField, FoldoutGroup("Audio")] AudioClip m_oDefeatMusic;

    enum eStates { None, LevelStart, EndLevel, LevelPlaying, BetweenLevels, GameOver }

    [SerializeField] Transform m_trBallSpawnPoint;
    [SerializeField] GameObject m_goBallPrefab;

    int m_iPoints;
    Ball m_oBall;

    List<Ball> m_aoBalls = new List<Ball>();
    List<PowerUp> m_aoPowerUps = new List<PowerUp>();

    int m_iNumberOfBreakables;
    int m_iCurrentLevel = -1;

    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oPointsText;
    [SerializeField, FoldoutGroup("UI")] GameObject m_goEndLevelMessage;
    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oEndLevelMessageText;
    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oTouchMessage;
    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oLevelText;
    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oNextLevelText;

    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oGameOverText;
    [SerializeField, FoldoutGroup("UI")] TMPro.TextMeshProUGUI m_oGameOverScore;
    [SerializeField, FoldoutGroup("UI")] GameObject m_goGameOver;

    [SerializeField] float m_fTransitionBlackHoldTime;

    [SerializeField] GameObject[] m_aoLevels;

    UnityEvent m_oOnLoadNextLevelEvent = new UnityEvent();

    eStates m_eState = eStates.None;
    GameObject m_goCurrrentLevel;

    protected override void Awake()
    {
        base.Awake();
        m_oPointsText.text = m_iPoints.ToString();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.O)) { endLevel(); }
#endif

        if (m_eState == eStates.None) { return; }

        handleStates();
    }

    void handleStates()
    {
        switch (m_eState)
        {
            case eStates.LevelStart:
                handleLevelStart();
            break;
            case eStates.EndLevel:
                handleEndLevel();
            break;
            case eStates.GameOver:
                handleGameOver();
                break;
        }
    }

    void handleLevelStart()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
#endif
        {
            Time.timeScale = 1;
            m_oTouchMessage.transform.parent.gameObject.SetActive(false);
            m_oBall.initMovement();
            m_eState = eStates.LevelPlaying;
        }
    }

    void handleEndLevel()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
#endif
        {
            m_goEndLevelMessage.SetActive(false);
            m_oTouchMessage.transform.parent.gameObject.SetActive(false);

            m_eState = eStates.BetweenLevels;

            FadeManager.Instance.OnFadeToBlacEndkEvent.AddListener(loadNextLevel);
            FadeManager.Instance.OnFadeToVisibleEndEvent.AddListener(initLevel);
            FadeManager.Instance.OnBlackEnd.AddListener(hideLevelText);
            FadeManager.Instance.play(FadeManager.eState.FadingToBlack, m_fTransitionBlackHoldTime);
        }
    }

    void handleGameOver()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
#endif
        {
            m_oTouchMessage.transform.parent.gameObject.SetActive(false);

            m_goGameOver.SetActive(false);

            m_eState = eStates.None;

            FadeManager.Instance.OnFadeToBlacEndkEvent.AddListener(showMenuScreen);

            FadeManager.Instance.play(FadeManager.eState.FadingToBlack, m_fTransitionBlackHoldTime);

            AudioManager.Instance.transitionTo(AudioManager.Instance.MainMenuMusic, FadeManager.Instance.FadeDuration + m_fTransitionBlackHoldTime / 2);
        }
    }

    void showMenuScreen()
    {
        Destroy(m_oBall.gameObject);
        clearExtraEntities();
        Destroy(m_goCurrrentLevel);

        FadeManager.Instance.OnFadeToBlacEndkEvent.RemoveListener(showMenuScreen);
        GameManager.Instance.initMainMenu();
    }

    void hideLevelText()
    {
        m_oNextLevelText.transform.parent.gameObject.SetActive(false);
        FadeManager.Instance.OnBlackEnd.RemoveListener(hideLevelText);
    }


    void loadNextLevel()
    {
        FadeManager.Instance.OnFadeToBlacEndkEvent.RemoveListener(loadNextLevel);

        if (GameManager.Instance.isInMainMenu())
        {
            GameManager.Instance.handleNewGameStart();
        }

        clearExtraEntities();

        if (m_goCurrrentLevel != null) { Destroy(m_goCurrrentLevel); }

        m_iCurrentLevel++;
        m_oLevelText.text = (m_iCurrentLevel + 1).ToString();
        m_oNextLevelText.text = "Level " + (m_iCurrentLevel + 1).ToString();

        m_oNextLevelText.transform.parent.gameObject.SetActive(true);

        if(m_oBall == null)
        {
            m_oBall = Instantiate(m_goBallPrefab, m_trBallSpawnPoint.position, Quaternion.identity, transform).GetComponent<Ball>();
        }

        m_oBall.gameObject.SetActive(true);
        m_oBall.transform.position = m_trBallSpawnPoint.position;

        m_iNumberOfBreakables = 0;
        m_goCurrrentLevel = Instantiate(m_aoLevels[m_iCurrentLevel], transform);
        m_oOnLoadNextLevelEvent.Invoke();
    }

    void initLevel()
    {
        m_oTouchMessage.text = m_sStartLevelTouchMessage;
        m_oTouchMessage.transform.parent.gameObject.SetActive(true);

        FadeManager.Instance.OnFadeToVisibleEndEvent.RemoveListener(initLevel);
        m_eState = eStates.LevelStart;
    }

    public void endLevel()
    {
        Time.timeScale = 0;

        if (m_iCurrentLevel == (m_aoLevels.Length - 1))
        {
            endGame(true);
        }
        else
        {
            m_eState = eStates.EndLevel;

            m_goEndLevelMessage.SetActive(true);
            m_oEndLevelMessageText.text = $"Score: {m_iPoints}";

            m_oTouchMessage.text = m_sEndLevelTouchMessage;
            m_oTouchMessage.transform.parent.gameObject.SetActive(true);
        }

        AudioManager.Instance.PlayAudioClipEffect(m_oVictoryMusic);
    }

    public void endGame(bool _bHasWon)
    {
        if(m_eState == eStates.GameOver) { return; }

        m_oBall.stopMovement();

        if (!_bHasWon) { AudioManager.Instance.PlayAudioClipEffect(m_oDefeatMusic); }

        m_oGameOverText.text = _bHasWon ? m_sGameOverWinMessage : m_sGameOverLoseMessage;
        m_goGameOver.SetActive(true);

        m_oGameOverScore.text = $"Final Score: {m_iPoints}";
        m_eState = eStates.GameOver;

        m_oTouchMessage.text = m_sGameOverTouchMessage;
        m_oTouchMessage.transform.parent.gameObject.SetActive(true);
    }

    public void resetBall()
    {
        m_eState = eStates.LevelStart;

        Time.timeScale = 0;

        clearExtraEntities();
        m_oBall.gameObject.SetActive(true);
        m_oBall.transform.position = m_trBallSpawnPoint.position;
        m_oBall.stopMovement();

        m_oTouchMessage.text = m_sStartLevelTouchMessage;
        m_oTouchMessage.transform.parent.gameObject.SetActive(true);
    }

    public void handleDestroyedBall(Ball _oBall)
    {
        if(m_oBall == _oBall)
        {
            m_oBall.stopMovement();
            m_oBall.gameObject.SetActive(false);
        }
        else
        {
            m_aoBalls.Remove(_oBall);
            Destroy(_oBall);
        }
    }

    public void addPoints(int _iScore)
    {
        m_iPoints += _iScore;
        m_oPointsText.text = m_iPoints.ToString();
    }

    public void addBreakable() { m_iNumberOfBreakables++; }
    public void removeBreakable()
    {
        m_iNumberOfBreakables--;
        if(m_iNumberOfBreakables <= 0)
        {
            endLevel();
        }
    }

    public void createExtraBall()
    {
        Ball oBall = Instantiate(m_goBallPrefab, m_trBallSpawnPoint.position, Quaternion.identity, transform).GetComponent<Ball>();
        oBall.transform.position = m_trBallSpawnPoint.position;
        oBall.initMovement();
        m_aoBalls.Add(oBall);
    }

    public void addPowerUp(PowerUp _oPowerUp) { m_aoPowerUps.Add(_oPowerUp); }
    public void removePowerUp(PowerUp _oPowerUp) { m_aoPowerUps.Remove(_oPowerUp); }

    void clearExtraEntities()
    {
        for(int i = 0; i < m_aoBalls.Count; i++)
        {
            Destroy(m_aoBalls[i].gameObject);
        }

        m_aoBalls.Clear();

        for (int i = 0; i < m_aoPowerUps.Count; i++)
        {
            Destroy(m_aoPowerUps[i].gameObject);
        }

        m_aoPowerUps.Clear();
    }

    public void startLevel0()
    {
        Player.Instance.init();

        m_iCurrentLevel = -1;
        m_iNumberOfBreakables = 0;
        m_iPoints = 0;
        m_oPointsText.text = "0";

        FadeManager.Instance.OnFadeToBlacEndkEvent.AddListener(loadNextLevel);
        FadeManager.Instance.OnFadeToVisibleEndEvent.AddListener(initLevel);
        FadeManager.Instance.OnBlackEnd.AddListener(hideLevelText);
        FadeManager.Instance.play(FadeManager.eState.FadingToBlack, m_fTransitionBlackHoldTime);
    }

    public UnityEvent OnLoadNextLevelEvent { get { return m_oOnLoadNextLevelEvent; } }

    public int NumberOfBalls { get { return 1 + m_aoBalls.Count; } }
    public bool isPlayState() { return m_eState == eStates.LevelPlaying; }
}
