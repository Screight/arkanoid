using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Singleton<Player>
{
    [SerializeField] AudioClip m_oDamageEffect;
    [SerializeField] AudioClip m_oLoseLifeEffect;
    [SerializeField] float m_fSpeed;
    [SerializeField, Range(20, 60)] float m_fMinAngle = 30;

    [SerializeField] Image m_oHealthFill;
    [SerializeField] TMPro.TextMeshProUGUI m_oLives;

    float m_fPaddleHalfSize;

    Rigidbody2D m_rb;

    Vector2 m_vScreenLimits;
    float m_fHalfSize;

    [SerializeField] float m_fMaxHealth;
    [SerializeField] int m_iMaxNumberOfLives;

    float m_fHealth;
    int m_iNumberOfLives = 3;

    bool m_bIsInit = false;

    public void init()
    {
        setHealth(m_fMaxHealth);
        setLives(m_iMaxNumberOfLives);

        transform.position = new Vector3(0, transform.position.y, transform.position.z);

        if (m_bIsInit) { return; }

        m_bIsInit = true;

        m_rb = GetComponent<Rigidbody2D>();
        m_fPaddleHalfSize = GetComponent<BoxCollider2D>().size.x / 2;

        float fCameraLeftLimitWorld = Camera.main.ScreenToWorldPoint(Vector2.zero).x;
        float fCameraRightLimitWorld = Camera.main.ScreenToWorldPoint(Vector2.right * Screen.width).x;

        m_vScreenLimits = new Vector2(fCameraLeftLimitWorld, fCameraRightLimitWorld);

        m_fHalfSize = GetComponent<BoxCollider2D>().size.x;
    }

    private void Update()
    {
#if UNITY_EDITOR
        m_rb.velocity = Vector2.right * Input.GetAxisRaw("Horizontal") * m_fSpeed;
#else
        if(Mathf.Abs(Input.acceleration.x) > 0.1f)
        {
            m_rb.velocity = Vector2.right * Input.acceleration.x * m_fSpeed;
        }
        else{ m_rb.velocity = Vector2.zero; }
#endif

        restrictMovementToScreen();
    }

    void restrictMovementToScreen()
    {
        float vPosX = 0;
        if(transform.position.x - m_fHalfSize < m_vScreenLimits.x)
        {
            vPosX = m_vScreenLimits.x + m_fHalfSize;
        }
        else if(transform.position.x + m_fHalfSize > m_vScreenLimits.y)
        {
            vPosX = m_vScreenLimits.y - m_fHalfSize;
        }

        if(vPosX != 0)
        {
            transform.position = new Vector3(vPosX, transform.position.y, transform.position.z);
        }
    }

    private void OnCollisionEnter2D(Collision2D _oCollision)
    {
        Ball oBall = _oCollision.gameObject.GetComponent<Ball>();

        if (oBall == null) { return; }

        Vector2 vNewDirection = getNewDirection(oBall.transform.position);
        oBall.changeDirection(vNewDirection);
    }

    Vector2 getNewDirection(Vector2 _oPos)
    {
        float fSignedDistanceToCenter = _oPos.x - transform.position.x;

        float fScale = fSignedDistanceToCenter / m_fPaddleHalfSize;
        float fNewAngle = 90 - fScale * (90 - m_fMinAngle);
        float fNewAngleRad = fNewAngle * Mathf.Deg2Rad;

        float fMinAngleRad = m_fMinAngle * Mathf.Deg2Rad;
        float fMaxnAngleRad = Mathf.PI - fMinAngleRad;

        fNewAngleRad = Math.Clamp(fNewAngleRad, fMinAngleRad, fMaxnAngleRad);

        return new Vector2(Mathf.Cos(fNewAngleRad), Mathf.Sin(fNewAngleRad));
    }

    public void damage(float _fValue)
    {
        setHealth(m_fHealth - _fValue);

        if (m_fHealth <= 0)
        {
            setLives(m_iNumberOfLives - 1);
            AudioManager.Instance.PlayAudioClipEffect(m_oLoseLifeEffect);
        }
        else
        {
            AudioManager.Instance.PlayAudioClipEffect(m_oDamageEffect);
            return;
        }

        if(m_iNumberOfLives <= 0) { LevelManager.Instance.endGame(false); }
        else
        {
            setHealth(m_fMaxHealth);
            LevelManager.Instance.resetBall();
        }
    }

    public void setHealth(float _fValue)
    {
        m_fHealth = Mathf.Clamp(_fValue, 0, m_fMaxHealth);
        m_oHealthFill.fillAmount = m_fHealth / m_fMaxHealth;
    }

    public void setLives(int _iValue)
    {
        m_iNumberOfLives = Mathf.Clamp(_iValue, 0, m_iMaxNumberOfLives);
        m_oLives.text = m_iNumberOfLives.ToString();
    }

    public float Health { get { return m_fHealth; } }
}
