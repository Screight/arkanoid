using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkTextComponent : MonoBehaviour
{
    [SerializeField] float m_fPeriod;
    bool m_bIsVisible;

    [SerializeField] TMPro.TextMeshProUGUI m_oText;
    Timer m_oTimer;

    private void OnEnable()
    {
        m_bIsVisible = true;
        m_oText.enabled = true;
        m_oTimer.Restart();
    }

    private void OnDisable()
    {
        if (TimerManager.IsNull) { return; }
        m_oTimer.Stop();
    }

    private void Awake()
    {
        if(m_oText == null) { m_oText = GetComponent<TMPro.TextMeshProUGUI>(); }
        
        m_oTimer = new Timer(m_fPeriod, false, true, null, blink, false);
    }

    void blink()
    {
        m_bIsVisible = !m_bIsVisible;
        m_oText.enabled = m_bIsVisible;
    }
}
