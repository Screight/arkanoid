using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{


    enum eState { MainMenu, Game }

    [SerializeField] GameObject[] m_aoGame;
    [SerializeField] GameObject m_oMenu;
    eState m_eState;

    protected override void Awake()
    {
        base.Awake();
        m_eState = eState.MainMenu;
        AudioManager.Instance.PlayBackgroundMusic(AudioManager.Instance.MainMenuMusic);
    }

    public void exitGame() { Application.Quit(); }
    public void startGame()
    {
        LevelManager.Instance.startLevel0();
        AudioManager.Instance.transitionTo(AudioManager.Instance.GameMusic, FadeManager.Instance.FadeDuration + 1f);
    }

    public void handleNewGameStart()
    {
        for (int i = 0; i < m_aoGame.Length; i++)
        {
            m_aoGame[i].gameObject.SetActive(true);
        }
        m_oMenu.gameObject.SetActive(false);
        m_eState = eState.Game;
    }

    public void initMainMenu()
    {
        for (int i = 0; i < m_aoGame.Length; i++)
        {
            m_aoGame[i].gameObject.SetActive(false);
        }
        m_oMenu.gameObject.SetActive(true);
        m_eState = eState.MainMenu;
    }

    public bool isInMainMenu() { return m_eState == eState.MainMenu; }
}
