using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static FadeManager;

public class FadeManager : Singleton<FadeManager>
{
    public enum eState { FadingToBlack, Black, FadingToVisible }

    [SerializeField] Image m_oBackground;
    Timer m_oTimer;
    [SerializeField] float m_fPeriod;
    float m_fTimeInBlack = -1;

    eState m_eState;
    UnityEvent m_onFadeToBlackEnd = new UnityEvent();
    UnityEvent m_onFadeToVisibleEnd = new UnityEvent();
    UnityEvent m_onBlackEnd = new UnityEvent();

    protected override void Awake()
    {
        base.Awake();

        m_oTimer = new Timer(m_fPeriod, false, false, onTick, onFadeEnd, false);
    }

    void onFadeEnd()
    {
        if (m_eState == eState.FadingToBlack)
        {
            Color oNewColor = m_oBackground.color;
            oNewColor.a = 1;
            m_oBackground.color = oNewColor;

            m_onFadeToBlackEnd.Invoke();
        }
        else if (m_eState == eState.FadingToVisible)
        {
            Color oNewColor = m_oBackground.color;
            oNewColor.a = 0;
            m_oBackground.color = oNewColor;

            m_oBackground.enabled = false;

            m_onFadeToVisibleEnd.Invoke();
        }

        if (m_eState == eState.FadingToBlack && m_fTimeInBlack > 0)
        {
            m_eState = eState.Black;
            m_oTimer.Period = m_fTimeInBlack;
            m_oTimer.Restart();
        }else
        {
            if(m_eState == eState.Black) { m_onBlackEnd.Invoke(); }
            m_eState = eState.FadingToVisible;
            m_oTimer.Period = m_fPeriod;
            m_oTimer.Restart();
        }
    }

    void onTick(float _fDelta)
    {
        if(m_eState == eState.Black) { return; }

        float fSpeed = 1 / m_fPeriod;
        float fSign = m_eState == eState.FadingToBlack ? 1 : -1;

        Color oNewColor = m_oBackground.color;
        oNewColor.a += fSign * fSpeed * _fDelta;
        m_oBackground.color = oNewColor; 
    }

    public void play(eState _eState, float _fTimeInBlack = -1)
    {
        m_oBackground.enabled = true;
        m_fTimeInBlack = _fTimeInBlack;
        m_oTimer.Period = m_fPeriod;
        m_oTimer.Restart();

        m_eState = _eState;

        Color oNewColor = m_oBackground.color;
        oNewColor.a = _eState == eState.FadingToBlack ? 0 : 1;
        m_oBackground.color = oNewColor;
    }

    public float FadeDuration { get { return m_fPeriod; } }

    public UnityEvent OnFadeToBlacEndkEvent { get { return m_onFadeToBlackEnd; } }
    public UnityEvent OnFadeToVisibleEndEvent { get { return m_onFadeToVisibleEnd; } }
    public UnityEvent OnBlackEnd { get { return m_onBlackEnd; } }
}
