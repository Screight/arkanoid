using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] float m_fDamage;
    [SerializeField] AudioClip m_oClip;
    [SerializeField] float m_fFallSpeed;
    Rigidbody2D m_oRb;
    enum eType { ExtraBall }
    [SerializeField] eType m_eType;

    private void Awake()
    {
        m_oRb = GetComponent<Rigidbody2D>();
        LevelManager.Instance.addPowerUp(this);
    }

    private void FixedUpdate()
    {
        Vector3 vNewPosition = transform.position + Vector3.down * m_fFallSpeed * Time.deltaTime;
        m_oRb.MovePosition(vNewPosition);
    }

    private void OnDestroy()
    {
        LevelManager.Instance.removePowerUp(this);
    }

    public void applyEffect()
    {
        AudioManager.Instance.PlayAudioClipEffect(m_oClip);

        switch (m_eType)
        {
            case eType.ExtraBall:
                LevelManager.Instance.createExtraBall();
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D _oCollision)
    {
        if (_oCollision.tag != "damage") { return; }

        Player.Instance.damage(m_fDamage);
        LevelManager.Instance.removePowerUp(this);
        Destroy(gameObject);
    }
}
