using System;
using System.Collections.Generic;

public class EventManager : Singleton<EventManager>
{
    enum eListenerAction { Add, Remove }
    struct LeftActions
    {
        Type m_oType;
        Delegate m_oDelegate;
        eListenerAction m_eAction;

        public LeftActions(Type _oType, Delegate _oDelegate, eListenerAction _eAction)
        {
            m_oType = _oType;
            m_oDelegate = _oDelegate;
            m_eAction = _eAction;
        }

        public Type Type { get { return m_oType; } }
        public Delegate Delegate {  get { return m_oDelegate; } }
        public eListenerAction Action { get { return m_eAction; } }
    }

    private readonly Dictionary<Type, List<Delegate>> m_aoListeners = new Dictionary<Type, List<Delegate>>();

    bool m_bIsSending = false;

    List<LeftActions> m_aoLeftActions = new List<LeftActions>();

    public void SendMessage(IEvent _oMessage)
    {
        List<Delegate> listeners = null;

        Dictionary<Type, List<Delegate>> oDictionary = new Dictionary<Type, List<Delegate>>(m_aoListeners);

        m_bIsSending = true;

        if (oDictionary.TryGetValue(_oMessage.GetType(), out listeners))
        {
            foreach (Delegate action in listeners)
            {
                action.DynamicInvoke(_oMessage);
            }
        }

        m_bIsSending = false;

        if(m_aoLeftActions.Count > 0) { resolveLeftActions(); }
    }

    public void resolveLeftActions()
    {
        for(int i = 0; i < m_aoLeftActions.Count; i++)
        {
            LeftActions oAction = m_aoLeftActions[i];

            if(oAction.Action == eListenerAction.Add)
            {
                addListener(oAction.Type, oAction.Delegate);
            }
            else
            {
                removeListener(oAction.Type, oAction.Delegate);
            }
        }
    }

    public void AddListener<T>(Action<T> _oListener)
    {
        if(m_bIsSending)
        {
            m_aoLeftActions.Add(new LeftActions(typeof(T), _oListener, eListenerAction.Add));
            return;
        }

        addListener(typeof(T), _oListener);
    }

    void addListener(Type _oType, Delegate _oDelegate)
    {
        List<Delegate> listeners = null;
        if (m_aoListeners.TryGetValue(_oType, out listeners))
        {
            listeners.Add(_oDelegate);
        }
        else
        {
            listeners = new List<Delegate> { _oDelegate };
            m_aoListeners.Add(_oType, listeners);
        }
    }

    public void RemoveListener<T>(Action<T> _oListener)
    {
        if (m_bIsSending)
        {
            m_aoLeftActions.Add(new LeftActions(typeof(T), _oListener, eListenerAction.Remove));
            return;
        }

        removeListener(typeof(T), _oListener);
    }

    void removeListener(Type _oType, Delegate _oDelegate)
    {
        List<Delegate> listeners = null;
        if (m_aoListeners.TryGetValue(_oType, out listeners))
        {
            listeners.Remove(_oDelegate);
        }
    }
}