using System;
using UnityEngine;


public interface IEvent
{
    public Type GetType();
}

public class StartLevel : IEvent
{

}