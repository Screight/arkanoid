using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField, FoldoutGroup("Music")] AudioClip m_oMainMenuMusic;
    [SerializeField, FoldoutGroup("Music")] AudioClip m_oGameMusic;

    [SerializeField] AudioMixer m_audioMixer;

    AudioSource m_backgroundMusicAudioSource;
    AudioSource m_generalAudioSource;

    float m_maxValue = 0;
    float m_minValue = -60;

    Timer m_oFadeTimer;
    AudioClip m_oNextMusic;

    enum eState { None, Down, Up }
    eState m_eState = eState.None;
    float m_fOriginalVolume;
    public void transitionTo(AudioClip _oMusic, float _fTransitionTime)
    {
        m_eState = eState.Down;
        m_oFadeTimer.Period = _fTransitionTime;
        m_fOriginalVolume = m_musicVolume;
        m_oNextMusic = _oMusic;

        m_oFadeTimer.Restart();
    }

    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(this.gameObject);

        m_backgroundMusicAudioSource = transform.GetChild(0).GetComponent<AudioSource>();
        m_generalAudioSource = transform.GetChild(1).GetComponent<AudioSource>();

        m_oFadeTimer = new Timer(0, false, false, onFadeTick, onFadeEnd, false);
    }

    void onFadeTick(float _fDelta)
    {
        float fSpeed = m_fOriginalVolume / m_oFadeTimer.Period;
        float fSign = m_eState == eState.Down ? -1 : 1;
        m_backgroundMusicAudioSource.volume += fSign * fSpeed * _fDelta;
    }

    void onFadeEnd()
    {
        if(m_eState == eState.Down)
        {
            m_eState = eState.Up;
            m_oFadeTimer.Restart();
            PlayBackgroundMusic(m_oNextMusic);
        }
    }

    public void PlayAudioClipEffect(AudioClip p_audioClip)
    {
        m_generalAudioSource.PlayOneShot(p_audioClip);
    }

    public void SetMasterVolumeTo(float p_value)
    {
        m_masterVolume = p_value;
        float value = 20 * Mathf.Log10(p_value);
        m_audioMixer.SetFloat("MasterVolume", value);
    }

    float m_masterVolume = 1;
    float m_effectVolume = 1;
    float m_musicVolume = 1;

    public float GetMasterVolume() { return m_masterVolume; }

    public float GetEffectVolume() { return m_effectVolume; }

    public float GetMusicVolume() { return m_musicVolume; }

    public void SetEffectsVolumeTo(float p_value)
    {
        m_effectVolume = p_value;
        float value = 20 * Mathf.Log10(p_value);
        m_audioMixer.SetFloat("EffectsVolume", value);
    }

    public void SetMusicVolumeTo(float p_value)
    {
        m_musicVolume = p_value;
        float value = 20 * Mathf.Log10(p_value);
        m_audioMixer.SetFloat("MusicVolume", value);
    }

    public void PlayBackgroundMusic(AudioClip p_audioClip)
    {
        m_backgroundMusicAudioSource.clip = p_audioClip;
        m_backgroundMusicAudioSource.Play();
    }

    public AudioSource BackgroundMusicAudioSource { get { return m_backgroundMusicAudioSource; } }

    public AudioClip MainMenuMusic { get { return m_oMainMenuMusic; } }
    public AudioClip GameMusic { get { return m_oGameMusic; } }
}
