using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] AudioClip m_oClip;
    [SerializeField, Range(0, 120)] float m_fAngleOpening;
    [SerializeField] float m_fBaseSpeed;
    [SerializeField] float m_fDamage;

    Rigidbody2D m_oRb;
    float m_fSpeed;
    Vector2 m_vDirection;

    private void Awake()
    {
        m_oRb = GetComponent<Rigidbody2D>();

        m_fSpeed = m_fBaseSpeed;
    }

    public void initMovement()
    {
        float fRandomNumber = Random.Range(0f, 1f);
        float fRandomAngle = 90 - m_fAngleOpening / 2 + fRandomNumber * m_fAngleOpening;
        float fAngleRadians = fRandomAngle * Mathf.Deg2Rad;

        m_oRb.velocity = new Vector2 (Mathf.Cos(fAngleRadians), Mathf.Sin(fAngleRadians)) * m_fSpeed;
    }

    public void stopMovement() { m_oRb.velocity = Vector2.zero; }

    public void changeDirection(Vector2 _vNewDirection)
    {
        m_oRb.velocity = _vNewDirection * m_fSpeed;
    }
    public Vector2 MovementDirection {  get { return m_oRb.velocity.normalized; } }

    private void OnCollisionEnter2D(Collision2D _oCollision)
    {
        AudioManager.Instance.PlayAudioClipEffect(m_oClip);
    }

    private void OnTriggerEnter2D(Collider2D _oCollision)
    {
        if(_oCollision.tag != "damage") { return; }

        LevelManager.Instance.handleDestroyedBall(this);

        if(LevelManager.Instance.NumberOfBalls == 1)
        {
            if (Player.Instance.Health - m_fDamage > 0) { LevelManager.Instance.resetBall(); }
            Player.Instance.damage(m_fDamage);
        }
    }
}
