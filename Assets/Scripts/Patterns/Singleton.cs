using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    static T m_oInstance = null;

    static public bool IsNull { get { return m_oInstance == null; } }

    public static T Instance
    {
        get
        {
            if (m_oInstance == null)
            {
                T t = GameObject.FindObjectOfType<T>(true);

                if(t == null)
                {
                    GameObject gO = new GameObject($"{typeof(T).Name} (singleton)");
                    m_oInstance = gO.AddComponent<T>();
                }
                else
                {
                    m_oInstance = t;
                }
            }
            return m_oInstance;
        }
        private set { }
    }

    protected virtual void Awake()
    {
        if (m_oInstance == null)
        {
            m_oInstance = this as T;
        }
    }

    private void OnApplicationQuit()
    {
        Destroy(this);
    }

}
