using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    [SerializeField] AudioClip m_oClip;
    [SerializeField] int m_fPointsOnDamage;
    [SerializeField] int m_fPointsOnDestroy;

    [SerializeField] float m_fBaseHealth;
    float m_fHealth;

    [SerializeField] PowerUp[] m_aoPowerUps;
    [SerializeField, Range(0, 1)] float m_fPowerUpChanceOnDeath;

    private void Start()
    {
        m_fHealth = m_fBaseHealth;
        LevelManager.Instance.addBreakable();
    }

    private void OnCollisionEnter2D(Collision2D _oCollision)
    {
        Ball oBall = _oCollision.gameObject.GetComponent<Ball>();
        if (oBall == null) { return; }

        m_fHealth--;

        bool bIsDestroyed = m_fHealth <= 0;

        int iPoints = bIsDestroyed ? m_fPointsOnDestroy : m_fPointsOnDamage;

        LevelManager.Instance.addPoints(iPoints);

        if (bIsDestroyed) { onDeath(); }
    }

    void onDeath()
    {
        bool bDropsPowerUp = m_fPowerUpChanceOnDeath > Random.Range(0f, 1f);
        if (bDropsPowerUp)
        {
            int iRandomNumber = Random.Range(0, m_aoPowerUps.Length);
            PowerUp oPowerUp = Instantiate(m_aoPowerUps[iRandomNumber], transform.position, Quaternion.identity, LevelManager.Instance.transform);
        }

        Destroy(gameObject);
        AudioManager.Instance.PlayAudioClipEffect(m_oClip);
        LevelManager.Instance.removeBreakable();
    }
}
