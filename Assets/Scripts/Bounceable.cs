using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncable : MonoBehaviour
{
    enum eFacingDirection { Horizontal, Vertical }
    [SerializeField] eFacingDirection m_eFacingDirection = eFacingDirection.Horizontal;

    private void OnCollisionEnter2D(Collision2D _oCollision)
    {
        Ball oBall = _oCollision.gameObject.GetComponent<Ball>();

        if(oBall == null || oBall.transform.position.y < transform.position.y)
        {
            return;
        }

        Vector2 vNewDirection = oBall.MovementDirection;

        if(m_eFacingDirection == eFacingDirection.Horizontal) { vNewDirection.y = -vNewDirection.y; }
        else { vNewDirection.x = -vNewDirection.x; }

        oBall.changeDirection(vNewDirection);
    }
}
