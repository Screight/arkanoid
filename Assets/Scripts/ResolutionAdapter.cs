using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionAdapter : MonoBehaviour
{
    [SerializeField] Vector2 m_vReferenceResolution = new Vector2(2560, 1440);
    Camera m_oCamera;

    private void Awake()
    {
        m_oCamera = GetComponent<Camera>();
    }

    private void Start()
    {
        adjustCameraSize();
    }

    void adjustCameraSize()
    {
        float fReferenceAspect = m_vReferenceResolution.y / m_vReferenceResolution.x;
        float fActualAspect = (float)Screen.width / (float)Screen.height;
        float fScale = fActualAspect / fReferenceAspect;

        m_oCamera.orthographicSize *= 1 / fScale;
    }
}
