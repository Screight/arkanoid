using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

public class Utilities : MonoBehaviour
{
#if UNITY_EDITOR
    [Button("construct row")]
    public void constructRow(uint _iAmount, GameObject _oPrefab, GameObject _oFirstObject, uint _iSeparation)
    {
        for (int i = 0; i < _iAmount; i++)
        {
            Vector2 vPos = _oFirstObject.transform.position + Vector3.right * (_iSeparation * (i + 1));
            GameObject oGO = PrefabUtility.InstantiatePrefab(_oPrefab) as GameObject;
            oGO.transform.position = vPos;
            oGO.transform.SetParent(_oFirstObject.transform.parent);
        }
    }

    [Button("construct column")]
    public void constructColumn(uint _iAmount, GameObject _oPrefab, GameObject _oFirstObject, uint _iSeparation)
    {
        for (int i = 0; i < _iAmount; i++)
        {
            Vector2 vPos = _oFirstObject.transform.position + Vector3.up * (_iSeparation * (i + 1));
            GameObject oGO = PrefabUtility.InstantiatePrefab(_oPrefab) as GameObject;
            oGO.transform.position = vPos;
            oGO.transform.SetParent(_oFirstObject.transform.parent);
        }
    }

    [Button("custor row")]
    public void constructRow(GameObject[] _aoPrefab, GameObject _oFirstObject, uint _iSeparation)
    {
        for (int i = 0; i < _aoPrefab.Length; i++)
        {
            if (_aoPrefab[i] == null) { continue; }

            Vector2 vPos = _oFirstObject.transform.position + Vector3.right * (_iSeparation * (i + 1));
            GameObject oGO = PrefabUtility.InstantiatePrefab(_aoPrefab[i]) as GameObject;
            oGO.transform.position = vPos;
            oGO.transform.SetParent(_oFirstObject.transform.parent);
        }
    }
#endif
}
