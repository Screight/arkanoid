using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPickUpController : MonoBehaviour
{
    [SerializeField] float m_fMinSpeed;

    CircleCollider2D m_oCollider;
    [SerializeField] TrailRenderer m_oTrailRenderer;

    enum eState { Idle, Slicing }
    eState m_eState = eState.Idle;

    Vector2 m_vDirection;

    private void Awake()
    {
        m_oCollider = GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        init();
        LevelManager.Instance.OnLoadNextLevelEvent.AddListener(clearTrail);
    }

    private void OnDestroy()
    {
        if (LevelManager.IsNull) { return; }
        LevelManager.Instance.OnLoadNextLevelEvent.RemoveListener(clearTrail);
    }

    void clearTrail() { m_oTrailRenderer.Clear(); }

    void init()
    {
        m_eState = eState.Idle;
        m_oCollider.enabled = false;
        m_oTrailRenderer.enabled = false;
        m_oTrailRenderer.Clear();
    }

    private void Update()
    {
        switch (m_eState)
        {
            case eState.Idle:
                handleIdleState();
                break;
            case eState.Slicing:
                handleSlicingState();
                break;

        }
    }

    void handleIdleState()
    {
        if (!getStartTouch()) { return; }

        m_eState = eState.Slicing;
        m_oTrailRenderer.Clear();
    }

    void handleSlicingState()
    {
        if (getEndTouch() || !LevelManager.Instance.isPlayState())
        {
            m_eState = eState.Idle;
            m_oCollider.enabled = false;
            m_oTrailRenderer.Clear();
            return;
        }

        Vector3 vNewPosition;

#if UNITY_EDITOR
        vNewPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#else
        vNewPosition = vNewPosition = Camera.main.ScreenToWorldPoint(getTouchPosition());
#endif
        vNewPosition.z = 0;

        m_vDirection = vNewPosition - transform.position;

        float fSpeed = m_vDirection.magnitude / Time.deltaTime;

        bool bIsEnabled = fSpeed > m_fMinSpeed;

        m_oCollider.enabled = bIsEnabled;
        m_oTrailRenderer.enabled = bIsEnabled;

        transform.position = vNewPosition;
    }

    Vector2 getTouchPosition()
    {
        return Input.touchCount > 0 ? Input.GetTouch(0).position : Vector2.zero;
    }

    bool getStartTouch()
    {
#if UNITY_EDITOR
        return Input.GetMouseButtonDown(0);
#else
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
#endif
    }

    bool getEndTouch()
    {
#if UNITY_EDITOR
        return Input.GetMouseButtonUp(0);
#else
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended;
#endif
    }   

    private void OnTriggerEnter2D(Collider2D _oCollision)
    {
        PowerUp oPowerUp = _oCollision.gameObject.GetComponent<PowerUp>();
        if(oPowerUp == null) { return; }

        oPowerUp.applyEffect();
        Destroy(oPowerUp.gameObject);
    }
}
